package edu.ntnu.idatt2003;

import edu.ntnu.idatt2003.model.ChaosGame;
import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.util.ChaosGameFileHandler;
import edu.ntnu.idatt2003.util.InvalidFileFormatException;
import java.io.IOException;
import java.util.Scanner;

/**
 * This class is responsible for handling the command line interface for the Chaos Game.
 */
public class ChaosGameCLI {
  private static Scanner scanner = new Scanner(System.in);
  private static ChaosGame chaosGame;

  public static void main(String[] args) {
    ChaosGameCLI cli = new ChaosGameCLI();
    cli.run();
  }

  /**
   * Runs the command line interface.
   */
  public void run() {
    Scanner scanner = new Scanner(System.in);
    boolean running = true;
    while (running) {
      printMenu();
      int choice = scanner.nextInt();
      switch (choice) {
        case 1:
          loadDescriptionFromFile();
          break;
        case 2:
          saveDescriptionToFile();
          break;
        case 3:
          runIterations();
          break;
        case 4:
          printAsciiFractal();
          break;
        case 5:
          running = false;
          break;
        default:
          System.out.println("Invalid choice");
      }
    }
  }

  private void printMenu() {
    System.out.println("Menu:");
    System.out.println("1. Load description from file");
    System.out.println("2. Save description to file");
    System.out.println("3. Run iterations");
    System.out.println("4. Print ASCII fractal to console");
    System.out.println("5. Exit");
    System.out.print("Enter your choice: ");
  }

  private void loadDescriptionFromFile() {
    System.out.println("Enter the file path: ");
    String filePath = scanner.nextLine();
    try {
      ChaosGameFileHandler handler = new ChaosGameFileHandler(filePath);
      ChaosGameDescription description = handler.readFile();
      chaosGame = new ChaosGame(description, 100, 100);
      System.out.println("Description loaded successfully");
    } catch (IOException | InvalidFileFormatException e) {
      System.out.println("Failed to load description from file: " + e.getMessage());
    }
  }

  private void saveDescriptionToFile() {
    if (chaosGame == null || chaosGame.getDescription() == null) {
      System.out.println("No description to save");
      return;
    }
    System.out.println("Enter the file path: ");
    String filePath = scanner.nextLine();

    try {
      ChaosGameFileHandler handler = new ChaosGameFileHandler(filePath);
      handler.writeToFile(filePath, chaosGame.getDescription());
      System.out.println("Description saved successfully");
    } catch (IOException e) {
      System.out.println("Failed to save description to file");
    }
  }

  private void runIterations() {
    if (chaosGame == null) {
      System.out.println("No description loaded");
      return;
    }

    System.out.println("Enter the number of iterations: ");
    int iterations = scanner.nextInt();
    scanner.nextLine();

    try {
      chaosGame.runSteps(iterations);
      System.out.println("Succsessfully ran " + iterations + " iterations");
    } catch (Exception e) {
      System.out.println("Failed to run iterations: " + e.getMessage());
    }
  }

  private void printAsciiFractal() {
    if (chaosGame == null || chaosGame.getCanvas() == null) {
      System.out.println("No fractal to print");
      return;
    }
    try {
      System.out.println(chaosGame.getCanvas().toAscii());
    } catch (Exception e) {
      System.out.println("Failed to print fractal: " + e.getMessage());
    }
  }
}