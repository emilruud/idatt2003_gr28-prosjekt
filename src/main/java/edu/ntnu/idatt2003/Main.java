package edu.ntnu.idatt2003;

import edu.ntnu.idatt2003.view.App;
import javafx.application.Application;

/**
 * The main class of the application.
 */
public class Main extends App {
  public static void main(String[] args) {
    Application.launch(App.class, args);
  }
}
