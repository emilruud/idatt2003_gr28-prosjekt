package edu.ntnu.idatt2003.util;

/**
 * Custom exception for invalid input.
 */
public class InvalidInputException extends Exception {
  private String invalidInput;

  public InvalidInputException(String message, String invalidInput) {
    super(message);
    this.invalidInput = invalidInput;
  }

  public String getInvalidInput() {
    return invalidInput;
  }
}
