package edu.ntnu.idatt2003.util;

public class InvalidFileFormatException extends Exception {
  public InvalidFileFormatException(String message) {
    super(message);
  }
}
