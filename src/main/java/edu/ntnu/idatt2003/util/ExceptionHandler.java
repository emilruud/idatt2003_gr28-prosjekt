package edu.ntnu.idatt2003.util;

import javafx.scene.control.Alert;

/**
 * This class is responsible for handling exceptions.
 */
public class ExceptionHandler {

  /**
   * Method to handle an invalid integer input.
   */
  public void showAlertIntegerInvalid(String input) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText("An error occurred");
    alert.setContentText("The input '" + input + "' is invalid. Please enter an integer number.");
    alert.showAndWait();
  }

  public void showAlertIntegerBelowOne(String input) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText("An error occurred");
    alert.setContentText("The input '" + input + "' is invalid. Please enter an integer above 1.");
    alert.showAndWait();
  }

  public void showAlertDoubleInvalid(String input) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText("An error occurred");
    alert.setContentText("The input '" + input + "' is invalid. Please enter a number.");
    alert.showAndWait();
  }

  public void showAlertFileError(String message) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("File Error");
    alert.setHeaderText("An error occurred");
    alert.setContentText(message);
    alert.showAndWait();
  }

  public void validatePostiviteInteger(int number) {
    if (number < 1) {
      throw new IllegalArgumentException("The number must be greater than 0.");
    }
  }


}
