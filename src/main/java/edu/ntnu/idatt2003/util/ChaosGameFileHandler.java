package edu.ntnu.idatt2003.util;


import edu.ntnu.idatt2003.model.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class is responsible for reading and writing ChaosGameDescriptions to and from files.
 */
public class ChaosGameFileHandler {
  private final String filePath;

  /**
   * Constructs a ChaosGameFileHandler with the given file path.
   *
   * @param filePath the path to the file
   */
  public ChaosGameFileHandler(String filePath) {
    this.filePath = filePath;
  }

  /**
   * Reads a file and returns a ChaosGameDescription.
   *
   * @return a ChaosGameDescription
   * @throws IOException if an I/O error occurs
   */
  public ChaosGameDescription readFile() throws IOException, InvalidFileFormatException {
    File file = new File(filePath);
    List<Transform2D> transforms = new ArrayList<>();
    Vector2D minCoords = null;
    Vector2D maxCoords = null;
    String type = null;

    try (Scanner scanner = new Scanner(file)) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine().trim();
        if (line.isEmpty() || line.startsWith("#")) {
          continue;  // Ignore comments and empty lines
        }

        if (line.contains("Type of transform")) {
          type = line.split("#")[0].trim();
          continue;
        }

        if (line.contains("Lower left")) {
          minCoords = parseVector2D(line.split("#")[0].trim());
          continue;
        }

        if (line.contains("Upper right")) {
          maxCoords = parseVector2D(line.split("#")[0].trim());
          continue;
        }

        try {
          if (type != null && type.equals("Julia")) {
            transforms = parseJulia(line.split("#")[0].trim());
          } else {
            Transform2D transform = parseAffineTransform(line.split("#")[0].trim());
            transforms.add(transform);
          }
        } catch (IllegalArgumentException e) {
          System.err.println("Error parsing transform from line: " + line);
        }
      }
    } catch (FileNotFoundException e) {
      System.err.println("Unable to open file: " + filePath);
      throw new IOException("File not found: " + filePath, e);
    }

    if (type == null || minCoords == null || maxCoords == null || transforms.isEmpty()) {
      throw new InvalidFileFormatException("Failed to properly parse the file: Missing essential data.");
    }

    return new ChaosGameDescription(transforms, minCoords, maxCoords);
  }


  /**
   * Parses a line of text to create an AffineTransform2D.
   *
   * @param line the line of text
   * @return an AffineTransform2D
   */
  public AffineTransform2D parseAffineTransform(String line) {
    String[] parts = line.split(",");
    ArrayList<Double> params = new ArrayList<>();
    for (String part : parts) {
      part = part.split("#")[0].trim();
      try {
        params.add(Double.parseDouble(part));
      } catch (NumberFormatException e) {
        System.err.println("Skipping invalid numeric data: " + part);
      }
    }

    if (params.size() < 6) {
      throw new IllegalArgumentException("Not enough valid numeric data to create an "
          + "AffineTransform2D: " + line);
    }

    return new AffineTransform2D(
        new Matrix2x2(params.get(0), params.get(1), params.get(2), params.get(3)),
        new Vector2D(params.get(4), params.get(5))
    );
  }

  /**
   * Parses a line of text to create a Vector2D.
   *
   * @param line the line of text
   * @return a Vector2D
   */
  public Vector2D parseVector2D(String line) {
    String[] parts = line.split("#")[0].trim().split(",");
    if (parts.length < 2) {
      return null;
    }
    try {
      double x = Double.parseDouble(parts[0].trim());
      double y = Double.parseDouble(parts[1].trim());
      return new Vector2D(x, y);
    } catch (NumberFormatException e) {
      System.err.println("Failed to parse vector from line: " + line + "; Error: "
          + e.getMessage());
      return null;
    }
  }

  /**
   * Parses a line of text to create a list of JuliaTransforms.
   *
   * @param line the line of text
   * @return a list of JuliaTransforms
   */
  private List<Transform2D> parseJulia(String line) {
    String[] parts = line.split(",");
    List<Transform2D> juliaTransforms = new ArrayList<>();
    juliaTransforms.add(new JuliaTransform(
        new Complex(Double.parseDouble(parts[0].trim()), Double.parseDouble(parts[1].trim())),
        1));
    juliaTransforms.add(new JuliaTransform(
        new Complex(Double.parseDouble(parts[0].trim()), Double.parseDouble(parts[1].trim())),
        -1));
    return juliaTransforms;
  }

  /**
   * Writes a ChaosGameDescription to a file.
   *
   * @param filePath the path to the file
   * @param description the ChaosGameDescription to write
   * @throws IOException if an I/O error occurs
   */
  public void writeToFile(String filePath, ChaosGameDescription description) throws IOException {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
      writer.write(description.toString());
    } catch (IOException e) {
      System.out.println("An error occurred: " + e.getMessage());
    }
  }
}

