package edu.ntnu.idatt2003.model;

import java.util.Locale;

/**
 * This class represents a 2D affine transformation of the form x -> Ax + b.
 * 'A' is a 2x2 matrix and 'b' is a 2D vector.
 */
public class AffineTransform2D implements Transform2D {
  private Matrix2x2 matrix;
  private Vector2D vector;

  /**
   * Constructs an AffineTransform2D with the given matrix and vector.
   *
   * @param matrix the matrix A
   * @param vector the vector b
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * Transforms the given point.
   *
   * @param point the vector to transform
   * @return the transformed vector
   */
  @Override
  public Vector2D transform(Vector2D point) {
    return matrix.multiply(point).add(vector);
  }

  public Matrix2x2 getMatrix() {
    return matrix;
  }

  public Vector2D getVector() {
    return vector;
  }

  /**
   * Returns a formatted string representation of the transformation.
   *
   * @return a formatted string representation of the transformation
   */
  public String toFormattedString() {
    double a00 = matrix.getA00();
    double a01 = matrix.getA01();
    double a10 = matrix.getA10();
    double a11 = matrix.getA11();

    double b0 = vector.getX0();
    double b1 = vector.getX1();

    return String.format(Locale.ENGLISH, "%.2f, %.2f, %.2f, %.2f, %.2f, %.2f",
        a00, a01, a10, a11, b0, b1);
  }

  public String getTransformType() {
    return "Affine2D";
  }
}
