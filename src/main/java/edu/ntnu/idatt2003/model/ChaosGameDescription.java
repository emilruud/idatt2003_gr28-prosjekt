package edu.ntnu.idatt2003.model;

import java.util.List;
import java.util.Objects;

/**
 * Represents a chaos game description.
 */
public class ChaosGameDescription {
  private final Vector2D minCoords;
  private final Vector2D maxCoords;
  private final List<Transform2D> transforms;

  /**
   * Constructs a ChaosGameDescription with the given transforms, minCoords and maxCoords.
   *
   * @param transforms the transforms of the chaos game
   * @param minCoords the minimum coordinates of the chaos game
   * @param maxCoords the maximum coordinates of the chaos game
   */
  public ChaosGameDescription(List<Transform2D> transforms, Vector2D minCoords,
                               Vector2D maxCoords) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
  }

  public List<Transform2D> getTransforms() {
    return transforms;
  }

  public Vector2D getMinCoords() {
    return minCoords;
  }

  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder(transforms.getFirst().getTransformType()
        + " # Type of transform" + "\n"
        + minCoords.getX0() + ", " + minCoords.getX1() + " # Lower left" + "\n"
        + maxCoords.getX0() + ", " + maxCoords.getX1() + " # Upper right");

    if (Objects.equals(transforms.getFirst().getTransformType(), "Julia")) {
      result.append("\n").append(transforms.getFirst().toFormattedString());
    } else if (Objects.equals(transforms.getFirst().getTransformType(), "Affine2D")) {
      for (Transform2D transform : transforms) {
        result.append("\n").append(transform.toFormattedString());
      }
    }
    return result.toString();
  }
}

