package edu.ntnu.idatt2003.model;

/**
 * This class represents a canvas for drawing chaos game fractals.
 * It has methods for putting and getting pixels, and for clearing the canvas.
 */
public class ChaosCanvas {
  private final int[][] canvas;
  private final int width;
  private final int height;
  private final Vector2D minCoords;
  private final Vector2D maxCoords;
  private AffineTransform2D transformCoordsToIndices;

  /**
   * Constructs a ChaosCanvas with the given width, height, minCoords and maxCoords.
   *
   * @param width the width of the canvas
   * @param height the height of the canvas
   * @param minCoords the minimum coordinates of the canvas
   * @param maxCoords the maximum coordinates of the canvas
   *
   */
  public ChaosCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    canvas = new int[height][width];
    initializeTransform();
  }

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  /**
   * Initializes the transformation from coordinates to indices.
   */
  private void initializeTransform() {
    double a01 = (height - 1) / (minCoords.getX1() - maxCoords.getX1());
    double a10 = (width - 1) / (maxCoords.getX0() - minCoords.getX0());

    Matrix2x2 matrix2x2 = new Matrix2x2(0, a01, a10, 0);

    double x = (height - 1) * maxCoords.getX1() / (maxCoords.getX1() - minCoords.getX1());
    double y = (width - 1) * minCoords.getX0() / (minCoords.getX0() - maxCoords.getX0());

    Vector2D vector2D = new Vector2D(x, y);
    transformCoordsToIndices = new AffineTransform2D(matrix2x2, vector2D);
  }

  /**
   * Returns the pixel at the given point.
   *
   * @param point the point to get the pixel from
   *
   * @return the pixel at the given point
   */
  public int getPixel(Vector2D point) {
    Vector2D transformedPoint = transformCoordsToIndices.transform(point);
    int i = (int) transformedPoint.getX0();
    int j = (int) transformedPoint.getX1();

    if (i >= 0 && i < height && j >= 0 && j < width) {
      return canvas[i][j];
    } else {
      return -1;
    }
  }

  /**
   * Puts a pixel at the given point.
   *
   * @param point the point to put the pixel at
   */
  public void putPixel(Vector2D point) {
    Vector2D transformedPoint = transformCoordsToIndices.transform(point);
    int i = (int) transformedPoint.getX0();
    int j = (int) transformedPoint.getX1();

    if (i >= 0 && i < height && j >= 0 && j < width) {
      canvas[i][j] = 1;
    }
  }

  /**
   * Returns the canvas array.
   *
   * @return the canvas array
   */
  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * Clears the canvas.
   */
  public void clear() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        canvas[i][j] = 0;
      }
    }
  }

  /**
   * Returns the canvas as an ASCII string.
   *
   * @return the canvas as an ASCII string
   */
  public String toAscii() {
    StringBuilder sb = new StringBuilder();
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        sb.append(getCanvasArray()[y][x] > 0 ? "X" : " ");
      }
      sb.append("\n");
    }
    return sb.toString();
  }
}
