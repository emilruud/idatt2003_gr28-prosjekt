package edu.ntnu.idatt2003.model;

import java.util.Locale;

/**
 * Class representing a Julia transformation.
 */
public class JuliaTransform implements Transform2D {
  private final Complex point;
  private final int sign;

  /**
   * Constructs a JuliaTransform object with the specified complex constant 'point'
   * and the chosen sign represented by the integer 'sign'.
   *
   * @param point The complex constant representing 'c' in the transformation formula.
   * @param sign  The sign of the transformation, should be +1 or -1.
   * @throws IllegalArgumentException If the sign is neither +1 nor -1.
   */
  public JuliaTransform(Complex point, int sign) {
    this.point = point;

    if (sign != 1 && sign != -1) {
      throw new IllegalArgumentException("Sign must be 1 or -1");
    }
    this.sign = sign;
  }

  /**
   * Transforms a complex vector 'z' according to the Julia transformation formula.
   * Computes ±sqrt(z - c) based on the chosen sign.
   *
   * @param z The complex vector to be transformed.
   * @return The transformed complex vector.
   */
  @Override
  public Vector2D transform(Vector2D z) {
    Vector2D result = (z.subtract(point));
    Complex complexResult = new Complex(result.getX0(), result.getX1());
    complexResult = complexResult.sqrt();
    return new Vector2D(complexResult.getX0() * sign, complexResult.getX1() * sign);
  }

  @Override
  public String getTransformType() {
    return "Julia";
  }

  @Override
  public String toFormattedString() {
    return String.format(Locale.ENGLISH, "%.5f, %.5f", point.getReal(), point.getImaginary());
  }

  public Complex getPoint() {
    return point;
  }
}
