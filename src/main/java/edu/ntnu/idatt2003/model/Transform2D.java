package edu.ntnu.idatt2003.model;

/**
 * This interface represents a 2D transformation.
 * It has a single method that takes a Vector2D and returns a new Vector2D.
 */
public interface Transform2D {
  /**
   * Transforms the given point.
   *
   * @param point the vector to transform
   * @return the transformed vector
   */

  Vector2D transform(Vector2D point);

  String getTransformType();

  String toFormattedString();
}

