package edu.ntnu.idatt2003.model;

/**
 * This class represents a 2D vector with x-component represented by x0 and
 * y-component represented by x1.
 */
public class Vector2D {
  private final double x0;
  private final double x1;

  /**
   * Constructs a Vector2D with the given x0 and x1 components.
   *
   * @param x0 the x-component of the vector
   * @param x1 the y-component of the vector
   */
  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Returns the x0 component of the vector.
   *
   * @return x0
   */
  public double getX0() {
    return x0;
  }

  /**
   * Returns the x1 component of the vector.
   *
   * @return x1
   */
  public double getX1() {
    return x1;
  }

  public Vector2D add(Vector2D other) {
    return new Vector2D(this.x0 +  other.x0, this.x1 + other.x1);
  }

  public Vector2D subtract(Vector2D other) {
    return new Vector2D(this.x0 -  other.x0, this.x1 - other.x1);
  }
}

