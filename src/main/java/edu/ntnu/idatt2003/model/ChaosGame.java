package edu.ntnu.idatt2003.model;

import edu.ntnu.idatt2003.controller.ChaosGameObserver;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Represents a chaos game.
 */
public class ChaosGame {
  private final ChaosCanvas canvas;
  private ChaosGameDescription description;
  private Vector2D currentPoint;
  private final Random random;
  private final List<ChaosGameObserver> observers;

  /**
   * Constructs a ChaosGame with the given ChaosGameDescription, width and height.
   *
   * @param description the description of the chaos game
   * @param width the width of the canvas
   * @param height the height of the canvas
   */
  public ChaosGame(ChaosGameDescription description, int width, int height) {
    this.description = description;
    this.canvas = new ChaosCanvas(width, height, description.getMinCoords(),
      description.getMaxCoords());
    this.currentPoint = new Vector2D(0, 0);
    this.random = new Random();
    this.observers = new ArrayList<>();
  }

  /**
   * Returns the canvas of the chaos game.
   *
   * @return the canvas
   */
  public ChaosCanvas getCanvas() {
    return canvas;
  }

  /**
   * Runs the chaos game for the given number of steps.
   *
   * @param steps the number of steps to run
   */
  public void runSteps(int steps) {
    if (description.getTransforms().isEmpty() || description.getTransforms() == null) {
      Logger.getGlobal().info("No transforms in description ");
      return;
    }
    for (int i = 0; i < steps; i++) {
      int randomIndex = random.nextInt(description.getTransforms().size());
      Transform2D transform = description.getTransforms().get(randomIndex);
      currentPoint = transform.transform(currentPoint);
      canvas.putPixel(currentPoint); // currentPoint is of type Vector2D
    }
    notifyObservers();
  }

  public ChaosGameDescription getDescription() {
    return this.description;
  }

  public ChaosGameDescription getChaosGameDescription() {
    return description;
  }

  public void setDescription(ChaosGameDescription description) {
    this.description = description;
  }

  public void addObserver(ChaosGameObserver observer) {
    observers.add(observer);
  }

  public void removeObserer(ChaosGameObserver observer) {
    observers.remove(observer);
  }

  private void notifyObservers() {
    for (ChaosGameObserver observer : observers) {
      observer.update();
    }
  }
}
