package edu.ntnu.idatt2003.model;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * This class is a factory for creating different standard fractals as ChaosGameDescriptions.
 */
public class ChaosGameDescriptionFactory {
  /**
   * Creates a ChaosGameDescription for the Sierpinski triangle.
   *
   * @return a ChaosGameDescription for the Sierpinski triangle
   */
  public static ChaosGameDescription createSierpinskiTriangle() {
    AffineTransform2D[] transforms = new AffineTransform2D[3];
    transforms[0] = new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5),
        new Vector2D(0.0, 0.0));
    transforms[1] = new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5),
        new Vector2D(0.5, 0.0));
    transforms[2] = new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5),
        new Vector2D(0.25, 0.5));

    Vector2D lowerLeft = new Vector2D(0.0, 0.0);
    Vector2D lowerRight = new Vector2D(1.0, 1.0);

    return new ChaosGameDescription(Arrays.asList(transforms), lowerLeft, lowerRight);
  }

  /**
   * Creates a ChaosGameDescription for the Barnsley fern.
   *
   * @return a ChaosGameDescription for the Barnsley fern
   */
  public  static ChaosGameDescription createBarnsleyFern() {
    AffineTransform2D[] transforms = new AffineTransform2D[4];
    transforms[0] = new AffineTransform2D(new Matrix2x2(0.0, 0.0, 0.0, 0.16),
        new Vector2D(0.0, 0.0));
    transforms[1] = new AffineTransform2D(new Matrix2x2(0.85, 0.04, -0.04, 0.85),
        new Vector2D(0.0, 1.6));
    transforms[2] = new AffineTransform2D(new Matrix2x2(0.2, -0.26, 0.23, 0.22),
        new Vector2D(0.0, 1.6));
    transforms[3] = new AffineTransform2D(new Matrix2x2(-0.15, 0.28, 0.26, 0.24),
        new Vector2D(0.0, 0.44));

    Vector2D lowerLeft = new Vector2D(-2.5, 0.0);
    Vector2D upperRight = new Vector2D(2.5, 10.0);

    return new ChaosGameDescription(Arrays.asList(transforms), lowerLeft, upperRight);
  }

  /**
   * Creates a ChaosGameDescription for the Julia set with the given complex number.
   *
   * @param c the complex number
   * @param sign the sign of the imaginary part of the complex number
   *
   * @return a ChaosGameDescription for the Julia set
   */
  public static ChaosGameDescription createJuliaSet(Complex c, int sign) {
    JuliaTransform[] transforms = new JuliaTransform[2];
    transforms[0] = new JuliaTransform(c, sign);
    transforms[1] = new JuliaTransform(c, -sign);
    Vector2D lowerLeft = new Vector2D(-1.6, -1.0);
    Vector2D upperRight = new Vector2D(1.6, 1.0);

    return new ChaosGameDescription(Arrays.asList(transforms), lowerLeft, upperRight);
  }
}
