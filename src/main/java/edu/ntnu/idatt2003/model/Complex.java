package edu.ntnu.idatt2003.model;


import static java.lang.Math.abs;

/**
 * Class representing a complex number.
 */
public class Complex extends Vector2D {

  /**
   * Constructs a complex number with the specified real and imaginary parts.
   *
   * @param realPart      The real part of the complex number.
   * @param imaginaryPart The imaginary part of the complex number.
   */
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
  }

  /**
   * Calculates the square root of the complex number.
   *
   * @return The square root of the complex number.
   */
  public Complex sqrt() {
    double absoluteZ = Math.sqrt(Math.pow(this.getX0(), 2) + Math.pow(this.getX1(), 2));
    return new Complex(Math.sqrt((absoluteZ + this.getX0()) / 2), this.getX1()
      / abs(this.getX1()) * Math.sqrt((absoluteZ - this.getX0()) / 2));
  }

  public double getReal() {
    return this.getX0();
  }

  public double getImaginary() {
    return this.getX1();
  }
}