package edu.ntnu.idatt2003.model;

/**
 * Class representing a 2x2 matrix.
 */
public class Matrix2x2 {
  private final double a00;
  private final double a01;
  private final double a10;
  private final double a11;

  /**
   * Constructs a 2x2 matrix with the specified elements.
   *
   * @param a00 The element in the first row and first column.
   * @param a01 The element in the first row and second column.
   * @param a10 The element in the second row and first column.
   * @param a11 The element in the second row and second column.
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * Multiplies the matrix with a vector.
   *
   * @param vector The vector to multiply with.
   * @return The resulting vector.
   */
  public Vector2D multiply(Vector2D vector) {
    return new Vector2D(
        a00 * vector.getX0() + a01 * vector.getX1(),
        a10 * vector.getX0() + a11 * vector.getX1());
  }

  public double getA00() {
    return a00;
  }

  public double getA01() {
    return a01;
  }

  public double getA10() {
    return a10;
  }

  public double getA11() {
    return a11;
  }
}

