package edu.ntnu.idatt2003.controller;

import edu.ntnu.idatt2003.util.ExceptionHandler;
import edu.ntnu.idatt2003.view.components.TextFields;

/**
 * This class is responsible for handling textfield events.
 */
public class TextFieldObserver implements Observer {
  private ExceptionHandler exceptionHandler = new ExceptionHandler();
  private FractalOperations fractalOperations;
  private TextFields textFields;

  /**
   * Constructs a TextFieldObserver with the given fractalOperations and textFields.
   *
   * @param fractalOperations the fractalOperations
   * @param textFields the textFields
   */
  public TextFieldObserver(FractalOperations fractalOperations, TextFields textFields) {
    this.fractalOperations = fractalOperations;
    this.textFields = textFields;
    textFields.attach(this);
  }

  /**
   * Updates the textfield observer depending on the textfield entered.
   *
   * @param textField the textfield entered
   */
  @Override
  public void update(String textField) {
    switch (textField) {
      case "addIterationInput":
        handleAddIterationInput();
        break;

      default:
        break;
    }
  }

  /**
   * Handles the add iteration input. Parses the input and adds the iterations to the fractal.
   * If the input is not an integer number, an alert is shown.
   */
  public void handleAddIterationInput() {
    String input = textFields.addIterationInput.getText();
    try {
      int iterations = Integer.parseInt(input);
      exceptionHandler.validatePostiviteInteger(iterations);
      fractalOperations.drawFractal(iterations);
    } catch (NumberFormatException ex) {
      exceptionHandler.showAlertIntegerInvalid(input);
    } catch (IllegalArgumentException ex) {
      exceptionHandler.showAlertIntegerBelowOne(input);
    }
    textFields.addIterationInput.clear();
  }
}
