package edu.ntnu.idatt2003.controller;

/**
 * This interface is responsible for handling observer events.
 */
public interface ChaosGameObserver {
  void update();
}
