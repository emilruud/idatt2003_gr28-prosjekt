package edu.ntnu.idatt2003.controller;

import edu.ntnu.idatt2003.model.AffineTransform2D;
import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.model.Complex;
import edu.ntnu.idatt2003.model.Matrix2x2;
import edu.ntnu.idatt2003.model.Transform2D;
import edu.ntnu.idatt2003.model.Vector2D;
import edu.ntnu.idatt2003.util.ChaosGameFileHandler;
import edu.ntnu.idatt2003.util.ExceptionHandler;
import edu.ntnu.idatt2003.util.InvalidFileFormatException;
import edu.ntnu.idatt2003.util.InvalidInputException;
import edu.ntnu.idatt2003.view.components.Buttons;
import edu.ntnu.idatt2003.view.components.TextFields;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * This class is responsible for handling button events.
 */
public class ButtonObserver implements Observer {
  private ExceptionHandler exceptionHandler = new ExceptionHandler();
  private Buttons buttons;
  private FractalOperations fractalOperations;
  private TextFields textFields;
  private Label iterationsLabel;
  private VBox inputContainer; // Add this line to include inputContainer

  /**
   * Constructs a ButtonObserver with the given buttons, fractalOperations,
   * textFields and iterationsLabel.
   *
   * @param buttons the buttons
   * @param fractalOperations the fractalOperations
   * @param textFields the textFields
   * @param iterationsLabel the iterationsLabel
   */
  public ButtonObserver(Buttons buttons, FractalOperations fractalOperations,
                        TextFields textFields, Label iterationsLabel, VBox inputContainer) {
    this.buttons = buttons;
    this.fractalOperations = fractalOperations;
    this.textFields = textFields;
    this.iterationsLabel = iterationsLabel;
    this.inputContainer = inputContainer; // Add this line to include inputContainer
    buttons.attach(this);
  }

  /**
   * Updates the button observer depending on the button pressed.
   *
   * @param button the button pressed
   */
  @Override
  public void update(String button) {
    switch (button) {
      case "btnAdd10":
        fractalOperations.drawFractal(10);
        break;

      case "btnAdd100":
        fractalOperations.drawFractal(100);
        break;

      case "btnAdd1000":
        fractalOperations.drawFractal(1000);
        break;

      case "btnReset":
        fractalOperations.resetIterations();
        break;

      case "btnAddInput":
        TextField addIterationInput = textFields.getAddIterationInput();
        String input = addIterationInput.getText();
        try {
          int iterations = Integer.parseInt(input);
          exceptionHandler.validatePostiviteInteger(iterations);
          fractalOperations.drawFractal(iterations);
        } catch (NumberFormatException ex) {
          exceptionHandler.showAlertIntegerInvalid(input);
        } catch (IllegalArgumentException ex) {
          exceptionHandler.showAlertIntegerBelowOne(input);
        }
        break;

      case "btnExitApplication":
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit Confirmation");
        alert.setHeaderText("Are you sure you want to exit the application?");
        alert.setContentText("Press OK to exit the application, or Cancel to stay.");

        var result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
          Platform.exit();
        }
        break;

      case "btnAddFractalFromFile":
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Fractal From File");
        File file = fileChooser.showOpenDialog(new Stage());
        if (file != null) {
          try {
            ChaosGameFileHandler fileHandler = new ChaosGameFileHandler(file.getPath());
            ChaosGameDescription description = fileHandler.readFile();
            fractalOperations.setFractalCustomAffine(description);
          } catch (IOException | InvalidFileFormatException e) {
            exceptionHandler.showAlertFileError(e.getMessage());
          }
        }
        break;

      case "btnSaveFractalToFile":
        FileChooser fileChooserSave = new FileChooser();
        fileChooserSave.setTitle("Save Fractal To File");
        File fileSave = fileChooserSave.showSaveDialog(new Stage());
        if (fileSave != null) {
          fractalOperations.saveFractalToFile(fileSave.getPath());
        }
        break;

      case "btnUpdateAll":

        try {
          double xmin = parseInput(textFields.getxMinInput());
          double ymin = parseInput(textFields.getyMinInput());
          double xmax = parseInput(textFields.getxMaxInput());
          double ymax = parseInput(textFields.getyMaxInput());

          String selectedFractal = fractalOperations.getChaosGame().getChaosGameDescription()
              .getTransforms().getFirst().getTransformType();
          if ("Julia".equals(selectedFractal)) {
            double realPart = parseInput(textFields.getRealPartInput());
            double imaginaryPart = parseInput(textFields.getImaginaryPartInput());
            fractalOperations.updateJuliaSet(new Complex(realPart, imaginaryPart), -1,
                new Vector2D(xmin, ymin), new Vector2D(xmax, ymax));
            fractalOperations.updateMinAndMaxFractal(new Vector2D(xmin, ymin),
                new Vector2D(xmax, ymax));
            fractalOperations.resetIterations();
          } else if ("Affine2D".equals(selectedFractal)) {
            updateAffineTransforms(inputContainer);
            fractalOperations.resetIterations();
          }
        } catch (InvalidInputException e) {
          exceptionHandler.showAlertDoubleInvalid(e.getInvalidInput());
        }
        break;
      default:
        break;
    }
  }

  private void updateAffineTransforms(VBox inputContainer) {
    List<Transform2D> transforms = new ArrayList<>();
    for (Node node : inputContainer.getChildren()) {
      if (node instanceof HBox) {
        HBox matrixVectorBox = (HBox) node;
        VBox matrixBox = (VBox) matrixVectorBox.getChildren().get(0);
        VBox vectorBox = (VBox) matrixVectorBox.getChildren().get(1);

        TextField matrixInput1 = (TextField) ((HBox) matrixBox.getChildren()
            .get(1)).getChildren().get(0);
        TextField matrixInput2 = (TextField) ((HBox) matrixBox.getChildren()
            .get(1)).getChildren().get(1);
        TextField matrixInput3 = (TextField) ((HBox) matrixBox.getChildren()
            .get(2)).getChildren().get(0);
        TextField matrixInput4 = (TextField) ((HBox) matrixBox.getChildren()
            .get(2)).getChildren().get(1);
        TextField vectorInput1 = (TextField) vectorBox.getChildren().get(1);
        TextField vectorInput2 = (TextField) vectorBox.getChildren().get(2);

        try {
          double m00 = parseInput(matrixInput1);
          double m01 = parseInput(matrixInput2);
          double m10 = parseInput(matrixInput3);
          double m11 = parseInput(matrixInput4);
          double v0 = parseInput(vectorInput1);
          double v1 = parseInput(vectorInput2);

          Transform2D transform = new AffineTransform2D(new Matrix2x2(m00, m01, m10, m11),
              new Vector2D(v0, v1));
          transforms.add(transform);
        } catch (InvalidInputException e) {
          exceptionHandler.showAlertDoubleInvalid(e.getInvalidInput());
        }
      }
    }

    double xmin;
    double ymin;
    double xmax;
    double ymax;

    try {
      xmin = Double.parseDouble(textFields.getxMinInput().getText());
      ymin = Double.parseDouble(textFields.getyMinInput().getText());
      xmax = Double.parseDouble(textFields.getxMaxInput().getText());
      ymax = Double.parseDouble(textFields.getyMaxInput().getText());
    } catch (NumberFormatException e) {
      // Set to default values if parsing fails
      if ("Barnsley Fern".equals(fractalOperations.getCurrentFractal())) {
        xmin = -2.5;
        ymin = 0;
        xmax = 2.5;
        ymax = 10;
      } else {
        xmin = 0;
        ymin = 0;
        xmax = 1;
        ymax = 1;
      }
    }

    Vector2D lowerLeft = new Vector2D(xmin, ymin);
    Vector2D upperRight = new Vector2D(xmax, ymax);

    ChaosGameDescription description = new ChaosGameDescription(transforms, lowerLeft, upperRight);
    fractalOperations.updateDescription(description);
  }

  private double parseInput(TextField textField) throws InvalidInputException {
    String input = textField.getText();
    try {
      return Double.parseDouble(input);
    } catch (NumberFormatException e) {
      throw new InvalidInputException("Invalid input: " + input, input);
    }
  }

  public void setInputContainer(VBox inputContainer) {
    this.inputContainer = inputContainer;
  }
}
