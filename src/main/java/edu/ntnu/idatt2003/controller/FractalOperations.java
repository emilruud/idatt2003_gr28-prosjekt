package edu.ntnu.idatt2003.controller;

import static edu.ntnu.idatt2003.model.ChaosGameDescriptionFactory.createJuliaSet;

import edu.ntnu.idatt2003.model.AffineTransform2D;
import edu.ntnu.idatt2003.model.ChaosGame;
import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.model.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.model.Complex;
import edu.ntnu.idatt2003.model.JuliaTransform;
import edu.ntnu.idatt2003.model.Vector2D;
import edu.ntnu.idatt2003.util.ChaosGameFileHandler;
import java.io.IOException;
import java.util.Arrays;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * This class is responsible for performing operations on the fractals.
 */
public class FractalOperations implements ChaosGameObserver {
  private ChaosGame chaosGame;
  private WritableImage writableImage;
  private ImageView imageView;
  private String currentFractal;

  /**
   * Constructs a FractalOperations object with the given ImageView.
   *
   * @param imageView the ImageView
  l
   */
  public FractalOperations(ImageView imageView) {
    this.imageView = imageView;
    this.currentFractal = "initialFractal";
    this.writableImage = new WritableImage(400, 400);
    this.imageView.setImage(writableImage);
    setBlankImageView();
  }

  public void drawFractal(int iterations) {
    chaosGame.runSteps(iterations);
  }

  /**
   * Resets the iterations of the fractal.
   */
  public void resetIterations() {
    if (chaosGame != null) {
      chaosGame.getCanvas().clear();
      this.chaosGame.addObserver(this);
      drawFractal(0);
    }
  }

  /**
   * Sets the fractal to Sierpinski Triangle.
   */
  public void setFractalToSierpinskiTriangle() {
    this.chaosGame = new ChaosGame(ChaosGameDescriptionFactory.createSierpinskiTriangle(),
      400, 400);
    this.chaosGame.addObserver(this);
    drawFractal(0);
  }

  /**
   * Sets the fractal to Julia Set.
   */
  public void setFractalToJuliaSet() {
    this.chaosGame = new ChaosGame(ChaosGameDescriptionFactory
        .createJuliaSet(new Complex(-0.74543,  0.11301),
      1), 400, 400);
    this.chaosGame.addObserver(this);
    drawFractal(0);
  }

  /**
   * Sets the fractal to Barnsley Fern.
   */
  public void setFractalToBransleysFern() {
    this.chaosGame = new ChaosGame(ChaosGameDescriptionFactory.createBarnsleyFern(), 400, 400);
    this.chaosGame.addObserver(this);
    drawFractal(0);
  }

  /**
   * Sets the fractal to custom affine.
   */
  public void setFractalCustomAffine(ChaosGameDescription description) {
    this.chaosGame = new ChaosGame(description, 400, 400);
    this.chaosGame.addObserver(this);
    drawFractal(0);
  }

  /**
   * Sets the fractal to custom Julia.
   */
  public void setBlankImageView() {
    AffineTransform2D[] defaultTransforms = new AffineTransform2D[0];
    Vector2D defaultLowerLeft = new Vector2D(0, 0);
    Vector2D defaultUpperRight = new Vector2D(0, 0);

    ChaosGameDescription description = new ChaosGameDescription(Arrays.asList(defaultTransforms),
        defaultLowerLeft, defaultUpperRight);
    this.chaosGame = new ChaosGame(description, 400, 400);
    this.chaosGame.addObserver(this);
    //drawFractal(0);
    updateImage();
  }

  /**
   * Updates the fractal to the given complex number.
   *
   * @param complex the complex number
   */
  public void updateC(Complex complex) {
    this.chaosGame = new ChaosGame(createJuliaSet(complex, -1), 400, 400);
    this.chaosGame.addObserver(this);
    drawFractal(0);
  }

  /**
   * Updates the fractal to the given matrix and vector.
   *
   * @param description the ChaosGameDescription
   */
  public void updateDescription(ChaosGameDescription description) {
    this.chaosGame = new ChaosGame(description, 400, 400);
    this.chaosGame.addObserver(this);
    drawFractal(0);
  }

  /**
   * Updates the Julia Set fractal.
   *
   * @param complex the complex number
   * @param sign the sign
   * @param minCoords the minimum coordinates
   * @param maxCoords the maximum coordinates
   */
  public void updateJuliaSet(Complex complex, int sign, Vector2D minCoords, Vector2D maxCoords) {
    JuliaTransform[] transforms = new JuliaTransform[2];
    transforms[0] = new JuliaTransform(complex, sign);
    transforms[1] = new JuliaTransform(complex, -sign);
    ChaosGameDescription newChaosGame = new ChaosGameDescription(Arrays.asList(transforms),
        minCoords, maxCoords);
    this.chaosGame = new ChaosGame(newChaosGame, 400, 400);
    drawFractal(0);
  }

  public String getCurrentFractal() {
    return currentFractal;
  }

  /**
   * Getter for the ChaosGame object.
   *
   * @return the ChaosGame object
   */
  public ChaosGame getChaosGame() {
    return chaosGame;
  }

  /**
   * Updates the min and max coordinates of the fractal.
   *
   * @param minCoords the minimum coordinates of the fractal
   * @param maxCoords the maximum coordinates of the fractal
   */
  public void updateMinAndMaxFractal(Vector2D minCoords, Vector2D maxCoords) {
    ChaosGameDescription newChaosGame = new ChaosGameDescription(
        chaosGame.getChaosGameDescription().getTransforms(), minCoords, maxCoords);
    this.chaosGame = new ChaosGame(newChaosGame, 400, 400);
    this.chaosGame.addObserver(this);
  }

  /**
   * Loads a fractal from a file.
   *
   * @param path the path to the file
   */
  public void saveFractalToFile(String path) {
    ChaosGameFileHandler fileHandler = new ChaosGameFileHandler(path);
    try {
      fileHandler.writeToFile(path, chaosGame.getChaosGameDescription());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Updates the image of the fractal.
   */
  public void updateImage() {
    PixelWriter pixelWriter = writableImage.getPixelWriter();
    int[][] canvasArray = chaosGame.getCanvas().getCanvasArray();
    for (int y = 0; y < chaosGame.getCanvas().getHeight(); y++) {
      for (int x = 0; x < chaosGame.getCanvas().getWidth(); x++) {
        int pixel = canvasArray[y][x];
        Color color = pixel == 1 ? Color.BLACK : Color.WHITE;
        pixelWriter.setColor(x, y, color);
      }
    }
  }

  public ChaosGameDescription getDescription() {
    return chaosGame.getDescription();
  }

  @Override
  public void update() {
    updateImage();
  }
}
