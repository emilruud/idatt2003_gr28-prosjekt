package edu.ntnu.idatt2003.controller;

/**
 * This interface is responsible for handling observer events.
 */
public interface Observer {

  /**
   * Updates the observer depending on the event.
   *
   * @param string the event
   */
  void update(String string);
}
