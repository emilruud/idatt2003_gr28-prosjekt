package edu.ntnu.idatt2003.controller;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class representing a subject in the observer pattern.
 */
public abstract class Subject {
  List<Observer> observers;

  /**
   * Constructor for the subject.
   */
  public Subject() {
    observers = new ArrayList<>();
  }

  /**
   * Attach an observer to the subject.
   *
   * @param observer The observer to attach.
   */
  public void attach(Observer observer) {
    observers.add(observer);
  }

  /**
   * Notify all observers of the subject.
   */
  public void notifyObservers(String string) {
    for (Observer observer : observers) {
      observer.update(string);
    }
  }
}
