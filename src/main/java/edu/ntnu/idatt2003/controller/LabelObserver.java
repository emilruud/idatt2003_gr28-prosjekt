package edu.ntnu.idatt2003.controller;

import edu.ntnu.idatt2003.view.components.Buttons;
import edu.ntnu.idatt2003.view.components.ComboBoxes;
import edu.ntnu.idatt2003.view.components.TextFields;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * This class is responsible for handling label events.
 */
public class LabelObserver implements Observer {
  private Buttons buttons;
  private ComboBoxes comboBoxes;
  private TextFields textFields;
  private Label iterationLabel;
  private int iterations = 0;

  /**
   * Constructs a LabelObserver with the given buttons, comboBoxes, textFields and iterationLabel.
   *
   * @param buttons the buttons
   * @param comboBoxes the comboBoxes
   * @param textFields the textFields
   * @param iterationLabel the iterationLabel
   */
  public LabelObserver(Buttons buttons, ComboBoxes comboBoxes, TextFields textFields,
                       Label iterationLabel) {
    this.buttons = buttons;
    this.comboBoxes = comboBoxes;
    this.textFields = textFields;
    this.iterationLabel = iterationLabel;
    buttons.attach(this);
    comboBoxes.attach(this);
    textFields.attach(this);
  }

  /**
   * Updates the label observer depending on action from other components.
   *
   * @param label the label updated by other components being pressed
   */
  @Override
  public void update(String label) {
    switch (label) {
      case "btnAdd10":
        iterations += 10;
        break;

      case "btnAdd100":
        iterations += 100;
        break;

      case "btnAdd1000":
        iterations += 1000;
        break;

      case "btnReset":
        iterations = 0;
        break;

      case "btnAddInput":
        TextField addIterationInput = textFields.getAddIterationInput();
        String input1 = addIterationInput.getText();
        if (!input1.isEmpty()) {
          try {
            int inputIterations = Integer.parseInt(input1);
            if (inputIterations >= 1) {
              iterations += inputIterations;
            }
          } catch (NumberFormatException ex) {
            System.out.println("Invalid input. Please enter an integer.");
          }
        }
        addIterationInput.clear();
        break;

      case "addIterationInput":
        String input = textFields.getAddIterationInput().getText();
        if (!input.isEmpty()) {
          try {
            int inputIterations = Integer.parseInt(input);
            if (inputIterations >= 1) {
              iterations += inputIterations;
            }
          } catch (NumberFormatException ex) {
            System.out.println("Invalid input. Please enter an integer.");
          }
        }
        break;

      case "Sierpinski Triangle":
        iterations = 0;
        break;

      case "Julia Set":
        iterations = 0;
        break;

      case "Barnsley Fern":
        iterations = 0;
        break;

      case "Fractal From File":
        iterations = 0;
        break;

      default:
        break;
    }
    // Update the label with the new number of iterations
    iterationLabel.setText("Iterations: " + iterations);
  }
}
