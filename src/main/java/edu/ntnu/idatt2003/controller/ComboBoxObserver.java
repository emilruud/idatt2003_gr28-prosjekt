package edu.ntnu.idatt2003.controller;

import edu.ntnu.idatt2003.view.components.ComboBoxes;

/**
 * This class is responsible for handling combobox events.
 */
public class ComboBoxObserver implements Observer {
  private ComboBoxes comboBoxes;
  private FractalOperations fractalOperations;

  /**
   * Constructs a ComboBoxObserver with the given comboBoxes and fractalOperations.
   *
   * @param comboBoxes the comboBoxes
   * @param fractalOperations the fractalOperations
   */
  public ComboBoxObserver(ComboBoxes comboBoxes, FractalOperations fractalOperations) {
    this.comboBoxes = comboBoxes;
    this.fractalOperations = fractalOperations;
    comboBoxes.attach(this);
  }

  /**
   * Updates the combobox observer when an option is pressed in a ComboBox.
   *
   * @param option the option pressed in the ComboBox
   */
  @Override
  public void update(String option) {
    switch (option) {
      case "Sierpinski Triangle":
        fractalOperations.setFractalToSierpinskiTriangle();
        break;

      case "Julia Set":
        fractalOperations.setFractalToJuliaSet();
        break;

      case "Barnsley Fern":
        fractalOperations.setFractalToBransleysFern();
        break;

      case "Fractal From File":
        fractalOperations.setBlankImageView();
        break;

      default:
        break;
    }
  }
}
