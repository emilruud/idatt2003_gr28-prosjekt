package edu.ntnu.idatt2003.view.scenes;


import edu.ntnu.idatt2003.controller.ButtonObserver;
import edu.ntnu.idatt2003.controller.ComboBoxObserver;
import edu.ntnu.idatt2003.controller.FractalOperations;
import edu.ntnu.idatt2003.controller.LabelObserver;
import edu.ntnu.idatt2003.controller.TextFieldObserver;
import edu.ntnu.idatt2003.model.AffineTransform2D;
import edu.ntnu.idatt2003.model.ChaosCanvas;
import edu.ntnu.idatt2003.model.JuliaTransform;
import edu.ntnu.idatt2003.model.Matrix2x2;
import edu.ntnu.idatt2003.model.Vector2D;
import edu.ntnu.idatt2003.view.components.Buttons;
import edu.ntnu.idatt2003.view.components.ComboBoxes;
import edu.ntnu.idatt2003.view.components.Labels;
import edu.ntnu.idatt2003.view.components.TextFields;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * This class is responsible for creating the scene of the application.
 */
public class Scene {
  private Label iterationsLabel;
  private Label maxLabel;
  private Label minLabel;
  private javafx.scene.Scene scene;
  private WritableImage writableImage;
  private ImageView imageView;
  private ChaosCanvas chaosCanvas;
  private Buttons buttons;
  private ComboBoxes comboBoxes;
  private Labels labels;
  private TextFields textFields;
  private FractalOperations fractalOperations;
  private VBox cbox; // Container for the C input fields
  private VBox inputBox; // Container for all input fields
  private VBox matrixVectorBox; // Container for the matrix and vector input fields
  private HBox topBox;
  private VBox inputContainer;
  private ButtonObserver buttonObserver;
  private ComboBoxObserver comboBoxObserver;
  private LabelObserver labelObserver;
  private TextFieldObserver textFieldObserver;
  private double startX;
  private double startY;
  private double endX;
  private double endY;
  private Rectangle selectionRectangle;

  /**
   * Constructor for the AffineScene class.
   */
  public Scene() {
    buttons = new Buttons();
    comboBoxes = new ComboBoxes();
    labels = new Labels();
    textFields = new TextFields();
    initializeComponents();
    fractalOperations = new FractalOperations(imageView);
    buttonObserver = new ButtonObserver(buttons, fractalOperations, textFields,
        iterationsLabel, inputContainer);
    comboBoxObserver = new ComboBoxObserver(comboBoxes, fractalOperations);
    labelObserver = new LabelObserver(buttons, comboBoxes, textFields, iterationsLabel);
    textFieldObserver = new TextFieldObserver(fractalOperations, textFields);

    // Add listener to ComboBox to handle dynamic addition of C input fields
    comboBoxes.getComboBoxFractal().getSelectionModel().selectedItemProperty()
        .addListener((observable, oldValue, newValue) -> {
          if ("Julia Set".equals(newValue)) {
            removeMatrixVectorInputBoxes();
            removeFractalFromFileBox();
            addSaveFractalToFileButton();
            addcInputbox();
            createMaxMinVectorInputAndUpdateAll();
            updateVectorInputFields();
            buttonObserver.setInputContainer(inputContainer);
          } else if ("Sierpinski Triangle".equals(newValue)) {
            removeMatrixVectorInputBoxes();
            removeCinputBox();
            removeFractalFromFileBox();
            addMatrixVectorInputBoxes();
            addSaveFractalToFileButton();
            createMaxMinVectorInputAndUpdateAll();
            updateVectorInputFields();
            buttonObserver.setInputContainer(inputContainer);
          } else if ("Barnsley Fern".equals(newValue)) {
            removeMatrixVectorInputBoxes();
            removeCinputBox();
            removeFractalFromFileBox();
            addMatrixVectorInputBoxes();
            createMaxMinVectorInputAndUpdateAll();
            addSaveFractalToFileButton();
            updateVectorInputFields();
            buttonObserver.setInputContainer(inputContainer);
          } else if ("Fractal From File".equals(newValue)) {
            removeCinputBox();
            removeMatrixVectorInputBoxes();
            removeMaxMinVectorInput();
            addSaveFractalToFileButton();
            addFractalFromFileBox();
          }
        });

  }

  private void initializeComponents() {
    // Creating the root AnchorPane

    // HBox for buttons
    HBox bottomBox = new HBox(50); // spacing between buttons
    topBox = new HBox(50); // spacing between buttons

    bottomBox.getStyleClass().add("button-box");
    topBox.getStyleClass().add("top-box");

    // Initialize buttons
    Button btnAdd10 = buttons.getBtnAdd10();
    Button btnAdd100 = buttons.getBtnAdd100();
    Button btnAdd1000 = buttons.getBtnAdd1000();
    Button btnReset = buttons.getBtnReset();
    Button btnAddInput = buttons.getBtnAddInput();
    Button btnExitApplication = buttons.getBtnExitApplication();

    // Initialize ComboBoxes
    ComboBox<String> comboBoxFractal = comboBoxes.getComboBoxFractal();
    //ComboBox<String> comboBoxColor = comboBoxes.getComboBoxColor();

    // Initialize Labels
    iterationsLabel = labels.getIterationLabel();

    maxLabel = labels.geMaxLabel();
    minLabel = labels.getMinLabel();

    // Initialize TextFields
    TextField addIterationInput = textFields.getAddIterationInput();

    // Set the style of the labels
    iterationsLabel.getStyleClass().add("iterationLabel");
    addIterationInput.getStyleClass().add("addIterationInput");
    btnExitApplication.getStyleClass().add("exitButton");

    HBox addInputBox = new HBox(0);
    addInputBox.getChildren().addAll(addIterationInput, btnAddInput);

    // Add buttons to the HBox
    bottomBox.getChildren().addAll(btnAdd10, btnAdd100, btnAdd1000,
        addInputBox, btnReset, iterationsLabel);
    bottomBox.setPadding(new Insets(10, 10, 10, 10));

    topBox.getChildren().addAll(comboBoxFractal);
    topBox.setPadding(new Insets(10, 10, 10, 10));

    // Initialize the ChaosCanvas and the WritableImage
    chaosCanvas = new ChaosCanvas(600, 400, new Vector2D(0, 0), new Vector2D(600, 400));
    writableImage = new WritableImage(600, 400);
    imageView = new ImageView(writableImage);

    BorderPane imagePane = new BorderPane();
    imagePane.setCenter(imageView);

    imageView.fitHeightProperty().bind(imagePane.heightProperty());
    imageView.fitWidthProperty().bind(imagePane.widthProperty());
    imageView.setPreserveRatio(true);

    inputBox = new VBox(0);

    AnchorPane root = new AnchorPane();

    // Add components to the root AnchorPane
    root.getChildren().addAll(topBox, imagePane, bottomBox, btnExitApplication, inputBox);

    // Anchor the topBox to the top of the pane
    AnchorPane.setTopAnchor(topBox, 10.0);
    AnchorPane.setLeftAnchor(topBox, 10.0);
    AnchorPane.setRightAnchor(topBox, 10.0);

    // Anchor the bottomBox to the bottom of the pane
    AnchorPane.setBottomAnchor(bottomBox, 10.0);
    AnchorPane.setLeftAnchor(bottomBox, 10.0);
    AnchorPane.setRightAnchor(bottomBox, 10.0);

    // Anchor the imagePane to the remaining space between top and bottom boxes
    AnchorPane.setTopAnchor(imagePane, 70.0);
    AnchorPane.setBottomAnchor(imagePane, 80.0);
    AnchorPane.setLeftAnchor(imagePane, 10.0);
    AnchorPane.setRightAnchor(imagePane, 10.0);

    // Anchor the btnExitApplication to the top right corner
    AnchorPane.setTopAnchor(btnExitApplication, 10.0);
    AnchorPane.setRightAnchor(btnExitApplication, 10.0);

    // Anchor the MinMaxBox to the top right corner
    AnchorPane.setTopAnchor(inputBox, 65.0);
    AnchorPane.setRightAnchor(inputBox, 30.0);

    selectionRectangle = new Rectangle();
    selectionRectangle.setFill(null);
    selectionRectangle.setStroke(Paint.valueOf("BLACK"));
    root.getChildren().add(selectionRectangle);

    // Add mouse event handlers to the ImageView
    imageView.setOnMousePressed(this::handleMousePressed);
    imageView.setOnMouseDragged(this::handleMouseDragged);
    imageView.setOnMouseReleased(this::handleMouseReleased);


    // Create a scene with the root and set the CSS style
    scene = new javafx.scene.Scene(root, 1400, 830);
    scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
  }

  private void addcInputbox() {
    if (cbox == null) {
      cbox = createCinputBox();
      inputBox.getChildren().addFirst(cbox);
    }
  }

  private void removeCinputBox() {
    if (cbox != null) {
      inputBox.getChildren().remove(cbox);
      cbox = null;
    }
  }

  private VBox createCinputBox() {
    Label clabel = new Label("Update constant C");
    Button resetToDefaultButton = new Button("Reset to Default fractal");

    TextField realPartInput = textFields.getRealPartInput();
    TextField imaginaryPartInput = textFields.getImaginaryPartInput();

    realPartInput.getStyleClass().add("cInput");
    imaginaryPartInput.getStyleClass().add("cInput");

    realPartInput.setPromptText("Real part of C");
    imaginaryPartInput.setPromptText("Imaginary part of C");

    JuliaTransform julia = (JuliaTransform) fractalOperations.getChaosGame()
        .getDescription().getTransforms().getFirst();

    realPartInput.setText(String.valueOf(julia.getPoint().getReal()));
    imaginaryPartInput.setText(String.valueOf(julia.getPoint().getImaginary()));

    VBox cbox = new VBox(5);
    cbox.getChildren().addAll(clabel, realPartInput, imaginaryPartInput, resetToDefaultButton);
    cbox.setPadding(new Insets(0, 0, 0, 0));

    resetToDefaultButton.setOnAction(event -> {
      fractalOperations.setFractalToJuliaSet();
      updateVectorInputFields();
    });

    return cbox;
  }

  private HBox createMatrixVectorInputBox(AffineTransform2D transform) {
    HBox matrixVectorBox = new HBox(15);
    matrixVectorBox.setPadding(new Insets(5));

    VBox matrixBox = new VBox(5);
    VBox vectorBox = new VBox(5);

    Label matrixLabel = new Label("Matrix");
    Label vectorLabel = new Label("Vector");

    matrixBox.getChildren().add(matrixLabel);
    vectorBox.getChildren().add(vectorLabel);

    TextField matrixInput1 = textFields.getMatrixInput1();
    TextField matrixInput2 = textFields.getMatrixInput2();
    TextField matrixInput3 = textFields.getMatrixInput3();
    TextField matrixInput4 = textFields.getMatrixInput4();
    TextField vectorInput1 = textFields.getVectorInput1();
    TextField vectorInput2 = textFields.getVectorInput2();

    matrixInput1.setText(String.valueOf(transform.getMatrix().getA00()));
    matrixInput2.setText(String.valueOf(transform.getMatrix().getA01()));
    matrixInput3.setText(String.valueOf(transform.getMatrix().getA10()));
    matrixInput4.setText(String.valueOf(transform.getMatrix().getA11()));
    vectorInput1.setText(String.valueOf(transform.getVector().getX0()));
    vectorInput2.setText(String.valueOf(transform.getVector().getX1()));

    matrixInput1.getStyleClass().add("matrixInput");
    matrixInput2.getStyleClass().add("matrixInput");
    matrixInput3.getStyleClass().add("matrixInput");
    matrixInput4.getStyleClass().add("matrixInput");
    vectorInput1.getStyleClass().add("matrixInput");
    vectorInput2.getStyleClass().add("matrixInput");

    HBox upperMatrixRow = new HBox(5);
    HBox lowerMatrixRow = new HBox(5);

    upperMatrixRow.getChildren().addAll(matrixInput1, matrixInput2);
    lowerMatrixRow.getChildren().addAll(matrixInput3, matrixInput4);

    matrixBox.getChildren().addAll(upperMatrixRow, lowerMatrixRow);
    vectorBox.getChildren().addAll(vectorInput1, vectorInput2);
    matrixVectorBox.getChildren().addAll(matrixBox, vectorBox);
    return matrixVectorBox;
  }

  private void addMatrixVectorInputBoxes() {
    if (matrixVectorBox == null) {
      matrixVectorBox = new VBox(5);
      matrixVectorBox.setPadding(new Insets(5));
      inputContainer = new VBox(5);
      inputContainer.setPadding(new Insets(0));

      Button addMoreButton = new Button("Add More");
      addMoreButton.setOnAction(event -> {
        HBox newInputBox = createMatrixVectorInputBox(new AffineTransform2D(
            new Matrix2x2(0, 0, 0, 0), new Vector2D(0, 0)));
        inputContainer.getChildren().add(0, newInputBox);
      });

      Button resetToDefaultButton = new Button("Reset to Default fractal");
      resetToDefaultButton.setOnAction(event -> {
        resetToDefaultFractal(inputContainer, addMoreButton, resetToDefaultButton);
        updateVectorInputFields();
      });

      resetToDefaultFractal(inputContainer, addMoreButton, resetToDefaultButton);
      matrixVectorBox.getChildren().add(inputContainer);
      inputBox.getChildren().addFirst(matrixVectorBox);

    }
  }

  private void addFractalFromFileBox() {
    Button btnAddFractalFromFile = buttons.getBtnAddFractalFromFile();
    if (btnAddFractalFromFile != null && !topBox.getChildren().contains(btnAddFractalFromFile)) {
      topBox.getChildren().add(btnAddFractalFromFile);
    }
  }

  private void removeFractalFromFileBox() {
    Button btnAddFractalFromFile = buttons.getBtnAddFractalFromFile();
    if (btnAddFractalFromFile != null && topBox.getChildren().contains(btnAddFractalFromFile)) {
      topBox.getChildren().remove(btnAddFractalFromFile);
    }
  }

  private void removeMatrixVectorInputBoxes() {
    if (matrixVectorBox != null) {
      inputBox.getChildren().remove(matrixVectorBox);
      matrixVectorBox = null;
    }
  }

  private void resetToDefaultFractal(VBox inputContainer, Button addMoreButton,
                                     Button resetToDefaultButton) {
    inputContainer.getChildren().clear();
    if ("Barnsley Fern".equals(comboBoxes.getComboBoxFractal().getValue())) {
      fractalOperations.setFractalToBransleysFern();
      AffineTransform2D[] transforms = fractalOperations.getDescription().getTransforms()
          .toArray(new AffineTransform2D[0]);
      inputContainer.getChildren().addAll(createMatrixVectorInputBox(transforms[0]),
          createMatrixVectorInputBox(transforms[1]), createMatrixVectorInputBox(transforms[2]),
          createMatrixVectorInputBox(transforms[3]), addMoreButton, resetToDefaultButton);
    } else if ("Sierpinski Triangle".equals(comboBoxes.getComboBoxFractal().getValue())) {
      fractalOperations.setFractalToSierpinskiTriangle();
      AffineTransform2D[] transforms = fractalOperations.getDescription().getTransforms()
          .toArray(new AffineTransform2D[0]);
      inputContainer.getChildren().addAll(createMatrixVectorInputBox(transforms[0]),
          createMatrixVectorInputBox(transforms[1]), createMatrixVectorInputBox(transforms[2]),
          addMoreButton, resetToDefaultButton);
    }
  }

  public void updateMinAndMax(Vector2D minCoords, Vector2D maxCoords) {
    fractalOperations.updateMinAndMaxFractal(minCoords, maxCoords);
  }

  /**
   * Adds the save fractal to file button to the top box.
   */
  public void addSaveFractalToFileButton() {
    Button saveFractalToFileButton = buttons.getBtnSaveFractalToFile();
    if (saveFractalToFileButton != null && !topBox.getChildren()
        .contains(saveFractalToFileButton)) {
      topBox.getChildren().add(saveFractalToFileButton);
    }
  }

  /**
   * Removes the save fractal to file button from the top box.
   */
  public void createMaxMinVectorInputAndUpdateAll() {
    TextField xmininput = textFields.getxMinInput();
    TextField ymininput = textFields.getyMinInput();
    TextField xmaxinput = textFields.getxMaxInput();
    TextField ymaxinput = textFields.getyMaxInput();

    // Ensure style classes are added to these text fields
    xmininput.getStyleClass().add("MaxMinInput");
    ymininput.getStyleClass().add("MaxMinInput");
    xmaxinput.getStyleClass().add("MaxMinInput");
    ymaxinput.getStyleClass().add("MaxMinInput");

    Vector2D minCoords = fractalOperations.getChaosGame().getDescription().getMinCoords();
    Vector2D maxCoords = fractalOperations.getChaosGame().getDescription().getMaxCoords();

    xmininput.setText(String.valueOf(minCoords.getX0()));
    ymininput.setText(String.valueOf(minCoords.getX1()));
    xmaxinput.setText(String.valueOf(maxCoords.getX0()));
    ymaxinput.setText(String.valueOf(maxCoords.getX1()));

    updateVectorInputFields();

    HBox minBox = new HBox(5);
    HBox maxBox = new HBox(5);
    minBox.getChildren().addAll(xmininput, ymininput);
    maxBox.getChildren().addAll(xmaxinput, ymaxinput);

    buttonObserver.setInputContainer(inputBox);

    Button updateAllButton = buttons.getBtnUpdateAll();

    VBox minMaxAll = new VBox(5);
    minMaxAll.getChildren().addAll(maxLabel, maxBox, minLabel, minBox, updateAllButton);

    inputBox.getChildren().add(minMaxAll);
  }

  /**
   * Removes the save fractal to file button from the top box.
   */
  public void removeMaxMinVectorInput() {
    if (inputBox.getChildren().size() > 1) {
      inputBox.getChildren().remove(1);
    } else if (inputBox.getChildren().size() == 1) {
      inputBox.getChildren().remove(0);
    }
  }

  /**
   * Updates the vector input fields with the current min and max coordinates.
   */
  public void updateVectorInputFields() {
    textFields.getxMinInput().setText(String.valueOf(fractalOperations.getChaosGame()
        .getDescription().getMinCoords().getX0()));
    textFields.getyMinInput().setText(String.valueOf(fractalOperations.getChaosGame()
        .getDescription().getMinCoords().getX1()));
    textFields.getxMaxInput().setText(String.valueOf(fractalOperations.getChaosGame()
        .getDescription().getMaxCoords().getX0()));
    textFields.getyMaxInput().setText(String.valueOf(fractalOperations.getChaosGame()
        .getDescription().getMaxCoords().getX1()));
  }

  private void handleMousePressed(javafx.scene.input.MouseEvent event) {
    startX = event.getX();
    startY = event.getY();
    selectionRectangle.setX(startX);
    selectionRectangle.setY(startY);
    selectionRectangle.setWidth(0);
    selectionRectangle.setHeight(0);
  }

  private void handleMouseDragged(javafx.scene.input.MouseEvent event) {
    endX = event.getX();
    endY = event.getY();
    selectionRectangle.setWidth(Math.abs(endX - startX));
    selectionRectangle.setHeight(Math.abs(endY - startY));
    selectionRectangle.setX(Math.min(startX, endX));
    selectionRectangle.setY(Math.min(startY, endY));
  }

  private void handleMouseReleased(javafx.scene.input.MouseEvent event) {
    endX = event.getX();
    endY = event.getY();
    if (Math.abs(endX - startX) > 5 && Math.abs(endY - startY) > 5) {
      applyZoom();
    }
    selectionRectangle.setWidth(0);
    selectionRectangle.setHeight(0);
  }

  private void applyZoom() {
    double imageWidth = imageView.getBoundsInLocal().getWidth();
    double imageHeight = imageView.getBoundsInLocal().getHeight();

    double xmin = fractalOperations.getChaosGame().getDescription().getMinCoords().getX0();
    double ymin = fractalOperations.getChaosGame().getDescription().getMinCoords().getX1();
    double xmax = fractalOperations.getChaosGame().getDescription().getMaxCoords().getX0();
    double ymax = fractalOperations.getChaosGame().getDescription().getMaxCoords().getX1();

    double newMinX = xmin + (startX / imageWidth) * (xmax - xmin);
    double newMaxX = xmin + (endX / imageWidth) * (xmax - xmin);
    double newMinY = ymin + (startY / imageHeight) * (ymax - ymin);
    double newMaxY = ymin + (endY / imageHeight) * (ymax - ymin);

    Vector2D newMinCoords = new Vector2D(Math.min(newMinX, newMaxX), Math.min(newMinY, newMaxY));
    Vector2D newMaxCoords = new Vector2D(Math.max(newMinX, newMaxX), Math.max(newMinY, newMaxY));

    fractalOperations.updateMinAndMaxFractal(newMinCoords, newMaxCoords);
    updateVectorInputFields();
    fractalOperations.drawFractal(100000000);  // Optionally redraw with initial iterations
  }

  /**
   * Sets up the stage with the scene.
   *
   * @param primaryStage the stage to set up
   */
  public void setUpStage(Stage primaryStage) {
    primaryStage.setTitle("ChaosGame");
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}
