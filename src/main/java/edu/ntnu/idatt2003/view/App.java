package edu.ntnu.idatt2003.view;

import edu.ntnu.idatt2003.view.scenes.Scene;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The main class of the application.
 */
public class App extends Application {
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    Scene scene = new Scene();
    scene.setUpStage(primaryStage);
  }
}
