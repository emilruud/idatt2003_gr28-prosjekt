package edu.ntnu.idatt2003.view.components;

import edu.ntnu.idatt2003.controller.Subject;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;

import java.util.logging.Logger;

/**
 * This class is responsible for creating ComboBoxes.
 */
public class ComboBoxes extends Subject {
  private ComboBox<String> comboBoxFractal;
  private ComboBox<String> comboBoxColor;

  /**
   * Constructs a ComboBoxes object.
   */
  public ComboBoxes() {
    initializeComboBoxes();
  }

  /**
   * Initializes the ComboBoxes.
   */
  private void initializeComboBoxes() {
    //ComboBox for choosing fractal
    comboBoxFractal = new ComboBox<>();
    comboBoxFractal.setItems(FXCollections.observableArrayList(
        "Sierpinski Triangle", "Barnsley Fern", "Julia Set", "Fractal From File"
    ));
    comboBoxFractal.setPromptText("Choose Fractal");
    comboBoxFractal.setOnAction(e -> {
      String selectedFractal = getComboBoxFractal().getValue();

      if ("Sierpinski Triangle".equals(selectedFractal)) {
        Logger.getGlobal().info("Sierpinski Triangle");
        notifyObservers("Sierpinski Triangle");

      } else if ("Julia Set".equals(selectedFractal)) {
        Logger.getGlobal().info("Julia Set");
        notifyObservers("Julia Set");

      } else if ("Barnsley Fern".equals(selectedFractal)) {
        Logger.getGlobal().info("Barnsley Fern");
        notifyObservers("Barnsley Fern");

      } else if ("Fractal From File".equals(selectedFractal)) {
        Logger.getGlobal().info("Fractal From File");
        notifyObservers("Fractal From File");
      }
    });

    //ComboBox for choosing color
    comboBoxColor = new ComboBox<>();
    comboBoxColor.setItems(FXCollections.observableArrayList(
        "Red", "Green", "Blue"
    ));
    comboBoxColor.setPromptText("Choose Color");
  }

  public ComboBox<String> getComboBoxFractal() {
    return comboBoxFractal;
  }

  public ComboBox<String> getComboBoxColor() {
    return comboBoxColor;
  }
}
