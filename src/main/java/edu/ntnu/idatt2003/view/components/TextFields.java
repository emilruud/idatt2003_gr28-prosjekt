package edu.ntnu.idatt2003.view.components;

import edu.ntnu.idatt2003.controller.Subject;
import javafx.scene.control.TextField;

/**
 * This class is responsible for creating text fields for the GUI.
 */
public class TextFields extends Subject {
  public TextField addIterationInput;
  private TextField xminInput;
  private TextField yminInput;
  private TextField xmaxInput;
  private TextField ymaxInput;
  private TextField realPartInput;
  private TextField imaginaryPartInput;
  private TextField matrixInput1;
  private TextField matrixInput2;
  private TextField matrixInput3;
  private TextField matrixInput4;
  private TextField vectorInput1;
  private TextField vectorInput2;

  /**
   * Constructs a TextFields object.
   */
  public TextFields() {
    initializeInputFields();
  }

  /**
   * Initializes the text fields.
   */
  public void initializeInputFields() {
    addIterationInput = new TextField();
    addIterationInput.setPromptText("Add iterations");
    addIterationInput.setOnAction(e -> {
      System.out.println("Iterations added");
      notifyObservers("addIterationInput");
    });

    xminInput = new TextField();
    xminInput.setPromptText("X min");
    yminInput = new TextField();
    yminInput.setPromptText("Y min");
    xmaxInput = new TextField();
    xmaxInput.setPromptText("X max");
    ymaxInput = new TextField();
    ymaxInput.setPromptText("Y max");

    realPartInput = new TextField();
    realPartInput.setPromptText("Real part");
    imaginaryPartInput = new TextField();
    imaginaryPartInput.setPromptText("Imaginary part");

    matrixInput1 = new TextField();
    matrixInput1.setPromptText("a00");
    matrixInput2 = new TextField();
    matrixInput2.setPromptText("a01");
    matrixInput3 = new TextField();
    matrixInput3.setPromptText("a10");
    matrixInput4 = new TextField();
    matrixInput4.setPromptText("a11");

    vectorInput1 = new TextField();
    vectorInput1.setPromptText("b0");
    vectorInput2 = new TextField();
    vectorInput2.setPromptText("b1");
  }

  public TextField getAddIterationInput() {
    return addIterationInput;
  }

  public TextField getxMinInput() {
    return xminInput;
  }

  public TextField getyMinInput() {
    return yminInput;
  }

  public TextField getxMaxInput() {
    return xmaxInput;
  }

  public TextField getyMaxInput() {
    return ymaxInput;
  }

  public TextField getRealPartInput() {
    return realPartInput;
  }

  public TextField getImaginaryPartInput() {
    return imaginaryPartInput;
  }

  public TextField getMatrixInput1() {
    return createMatrixInputField("a00");
  }

  public TextField getMatrixInput2() {
    return createMatrixInputField("a01");
  }

  public TextField getMatrixInput3() {
    return createMatrixInputField("a10");
  }

  public TextField getMatrixInput4() {
    return createMatrixInputField("a11");
  }

  public TextField getVectorInput1() {
    return createMatrixInputField("b0");
  }

  public TextField getVectorInput2() {
    return createMatrixInputField("b1");
  }

  private TextField createMatrixInputField(String promptText) {
    TextField textField = new TextField();
    textField.setPromptText(promptText);
    return textField;
  }

}

