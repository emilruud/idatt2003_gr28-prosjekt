package edu.ntnu.idatt2003.view.components;

import javafx.scene.control.Label;

/**
 * This class is responsible for creating labels for the GUI.
 */
public class Labels {
  private Label iterationLabel;
  private Label maxLabel;
  private Label minLabel;

  /**
   * Constructs a Labels object.
   */
  public Labels() {
    initializeLabels();
  }

  /**
   * Initializes the labels.
   */
  public void initializeLabels() {
    iterationLabel = new Label();
    iterationLabel.setText("Iterations: 0");
    minLabel = new Label();
    minLabel.setText("Change Min vector");
    maxLabel = new Label();
    maxLabel.setText("Change Max vector");
  }

  public Label getIterationLabel() {
    return iterationLabel;
  }

  public Label getMinLabel() {
    return minLabel;
  }

  public Label geMaxLabel() {
    return maxLabel;
  }
}

