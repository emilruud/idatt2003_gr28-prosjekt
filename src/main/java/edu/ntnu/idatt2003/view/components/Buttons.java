package edu.ntnu.idatt2003.view.components;


import edu.ntnu.idatt2003.controller.Subject;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

import java.util.logging.Logger;

/**
 * This class is responsible for creating buttons.
 */
public class Buttons extends Subject {
  private Button btnAdd10;
  private Button btnAdd100;
  private Button btnAdd1000;
  private Button btnReset;
  private Button btnAddInput;
  private Button btnAddFractalFromFile;
  private Button btnSaveFractalToFile;
  private Button btnExitApplication;
  private Button btnUpdateAll;

  /**
   * Constructs a Buttons object.
   */
  public Buttons() {
    initializeButtons();
  }

  /**
   * Initializes the buttons.
   */
  private void initializeButtons() {
    btnAdd10 = new Button("Add 10");
    btnAdd10.setOnAction(e -> {
      Logger.getGlobal().info("Add 10");
      notifyObservers("btnAdd10");
    });

    btnAdd100 = new Button("Add 100");
    btnAdd100.setOnAction(e -> {
      Logger.getGlobal().info("Add 100");
      notifyObservers("btnAdd100");
    });

    btnAdd1000 = new Button("Add 1000");
    btnAdd1000.setOnAction(e -> {
      Logger.getGlobal().info("Add 1000");
      notifyObservers("btnAdd1000");
    });

    btnReset = new Button("Reset");
    btnReset.setOnAction(e -> {
      Logger.getGlobal().info("Reset");
      notifyObservers("btnReset");
    });

    btnAddInput = new Button("Add");
    btnAddInput.setOnAction(e -> {
      Logger.getGlobal().info("Add Input");
      notifyObservers("btnAddInput");
    });

    btnAddFractalFromFile = new Button("Load Fractal From File");
    btnAddFractalFromFile.setOnAction(e -> {
      Logger.getGlobal().info("Load Fractal From File");
      notifyObservers("btnAddFractalFromFile");
    });

    btnExitApplication = new Button("Exit");
    btnExitApplication.setOnAction(e -> {
      Logger.getGlobal().info("Exit Application");
      notifyObservers("btnExitApplication");
    });

    btnSaveFractalToFile = new Button("Save Fractal To File");
    btnSaveFractalToFile.setOnAction(e -> {
      Logger.getGlobal().info("Save Fractal To File");
      notifyObservers("btnSaveFractalToFile");
    });

    btnUpdateAll = new Button("Update all values");
    btnUpdateAll.setOnAction(e -> {
      Logger.getGlobal().info("Update all values");
      notifyObservers("btnUpdateAll");
    });
  }

  public Button getBtnAdd10() {
    return btnAdd10;
  }

  public Button getBtnAdd100() {
    return btnAdd100;
  }

  public Button getBtnAdd1000() {
    return btnAdd1000;
  }

  public Button getBtnReset() {
    return btnReset;
  }

  public Button getBtnAddInput() {
    return btnAddInput;
  }

  public Button getBtnAddFractalFromFile() {
    return btnAddFractalFromFile;
  }

  public Button getBtnExitApplication() {
    return btnExitApplication;
  }

  public Button getBtnSaveFractalToFile() {
    return btnSaveFractalToFile;
  }

  public Button getBtnUpdateAll() {
    return btnUpdateAll;
  }
}

