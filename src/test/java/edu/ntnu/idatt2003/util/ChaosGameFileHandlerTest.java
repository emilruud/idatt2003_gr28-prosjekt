package edu.ntnu.idatt2003.util;

import edu.ntnu.idatt2003.model.*;
import edu.ntnu.idatt2003.util.InvalidFileFormatException;
import edu.ntnu.idatt2003.util.ChaosGameFileHandler;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.util.List;

/**
 * Test class for the ChaosGameFileHandler class.
 */
public class ChaosGameFileHandlerTest {
  private static final String TEST_FILE_PATH = "testChaosGameDescription.txt";
  private ChaosGameFileHandler fileHandler;

  /**
   * Set up a new ChaosGameFileHandler object before each test.
   */
  @BeforeEach
  public void setup() {
    fileHandler = new ChaosGameFileHandler(TEST_FILE_PATH);
  }

  /**
   * Clean up after each test.
   */
  @AfterEach
  public void tearDown() {
    File file = new File(TEST_FILE_PATH);
    if (file.exists()) {
      file.delete();
    }
  }

  @Nested
  @DisplayName("Positive tests")
  class PositiveChaosGameFileHandlerTest {

    /**
     * Test reading a file with affine transforms.
     * @throws IOException if an I/O error occurs.
     * @throws InvalidFileFormatException if the file is not properly formatted.
     */
    @Test
    void testReadFileAffine() throws IOException, InvalidFileFormatException {
      createTestFileAffine();
      ChaosGameDescription description = fileHandler.readFile();
      assertNotNull(description);
      assertEquals(.0, description.getMinCoords().getX0());
      assertEquals(.0, description.getMinCoords().getX1());
      assertEquals(1.0, description.getMaxCoords().getX0());
      assertEquals(1.0, description.getMaxCoords().getX1());
      assertFalse(description.getTransforms().isEmpty());
      assertInstanceOf(AffineTransform2D.class, description.getTransforms().getFirst());
      assertEquals("Affine2D", description.getTransforms().getFirst().getTransformType());
    }

    /**
     * Test reading a file with a Julia transform.
     * @throws IOException if an I/O error occurs.
     * @throws InvalidFileFormatException if the file is not properly formatted.
     */
    @Test
    void testReadFileJulia() throws IOException, InvalidFileFormatException {
      createTestFileJulia();
      ChaosGameDescription description = fileHandler.readFile();
      assertNotNull(description);
      assertEquals(0.0, description.getMinCoords().getX0());
      assertEquals(0.0, description.getMinCoords().getX1());
      assertEquals(1.0, description.getMaxCoords().getX0());
      assertEquals(1.0, description.getMaxCoords().getX1());
      assertFalse(description.getTransforms().isEmpty());
      assertInstanceOf(JuliaTransform.class, description.getTransforms().getFirst());
      assertEquals("Julia", description.getTransforms().getFirst().getTransformType());
    }

    /**
     * Test writing a ChaosGameDescription to a file.
     * @throws IOException if an I/O error occurs.
     * @throws InvalidFileFormatException if the file is not properly formatted.
     */
    @Test
    public void testWriteToFile() throws IOException, InvalidFileFormatException {
      ChaosGameDescription description = new ChaosGameDescription(
          List.of(new AffineTransform2D(new Matrix2x2(1, 0, 0, 1), new Vector2D(0, 0))),
          new Vector2D(-1, -1),
          new Vector2D(1, 1)
      );

      String filePath = "outputTestFile.txt";
      fileHandler.writeToFile(filePath, description);

      File outputFile = new File(filePath);
      assertTrue(outputFile.exists());

      ChaosGameFileHandler readHandler = new ChaosGameFileHandler(filePath);
      ChaosGameDescription readDescription = readHandler.readFile();
      assertNotNull(readDescription);
      outputFile.delete();  // Clean up after test
    }

    /**
     * Create a test file with affine transforms.
     */
    private void createTestFileAffine() {
      try (PrintWriter out = new PrintWriter(new FileWriter(TEST_FILE_PATH))) {
        out.println("Affine2D # Type of transform");
        out.println(".0, .0 # Lower left");
        out.println("1.0, 1.0 # Upper right");
        out.println("0.5, 0, 0, 0.5, 0, 0 # 1st transform");
        out.println("0.5, 0, 0, 0.5, 0.25, 0.5 # 2nd transform");
        out.println("0.5, 0, 0, 0.5, 0.5, 0 # 3rd transform");
      } catch (IOException e) {
        fail("Failed to create test file.");
      }
    }


    /**
     * Create a test file with a Julia transform.
     */
    private void createTestFileJulia() {
      try (PrintWriter out = new PrintWriter(new FileWriter(TEST_FILE_PATH))) {
        out.println("Julia # Type of transform");
        out.println("0.0, 0.0 # Lower left");
        out.println("1.0, 1.0 # Upper right");
        out.println("0.5, 0.5 #");
      } catch (IOException e) {
        fail("Failed to create test file.");
      }
    }



  @Nested
  @DisplayName("Negative tests")
  class NegativeChaosGameFileHandlerTest {

    /**
     * Test reading a file with invalid data.
     * @throws IOException if an I/O error occurs.
     */
    @Test
    public void testReadFileInvalidData() throws IOException {
      createInvalidDataFile();
      Exception exception = assertThrows(InvalidFileFormatException.class, () -> fileHandler.readFile());
      assertTrue(exception.getMessage().contains("Failed to properly parse the file"));
    }

    /**
     * Test reading an empty file.
     * @throws IOException if an I/O error occurs.
     */
    @Test
    public void testReadEmptyFile() throws IOException {
      createEmptyFile();
      Exception exception = assertThrows(InvalidFileFormatException.class, () -> fileHandler.readFile());
      assertTrue(exception.getMessage().contains("Missing essential data"));
    }

    /**
     * Create a file with invalid data.
     */
    private void createInvalidDataFile() {
      try (PrintWriter out = new PrintWriter(new FileWriter(TEST_FILE_PATH))) {
        out.println("InvalidTransform # Type of transform");
        out.println(".0, .0 # Lower left");
        out.println("1.0, 1.0 # Upper right");
        out.println("invalid, data, for, transform # Invalid transform data");
      } catch (IOException e) {
        fail("Failed to create test file.");
      }
    }

    /**
     * Create an empty file.
     * @throws IOException if an I/O error occurs.
     */
    private void createEmptyFile() throws IOException {
      new PrintWriter(TEST_FILE_PATH).close();
    }
  }
  }
}
