package edu.ntnu.idatt2003.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for the ChaosGame class.
 */
public class ChaosGameTest {
  private ChaosGame chaosGame;
  private ChaosGameDescription description;
  private Vector2D minCoords;
  private Vector2D maxCoords;

  /**
   * Sets up the test objects before each test.
   */
  @BeforeEach
  void setUp() {
    // Define the bounds and a simple transform for the test
    minCoords = new Vector2D(-1.0, -1.0);
    maxCoords = new Vector2D(1.0, 1.0);
    Transform2D dummyTransform = new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.1, 0.1));
    List<Transform2D> transforms = List.of(dummyTransform);

    description = new ChaosGameDescription(transforms, minCoords, maxCoords);
    chaosGame = new ChaosGame(description, 100, 100);
  }

  /**
   * Tests the initial state of the ChaosGame object.
   */
  @Test
  void testInitialSetup() {
    assertNotNull(chaosGame.getCanvas());
    assertEquals(100, chaosGame.getCanvas().getWidth());
    assertEquals(100, chaosGame.getCanvas().getHeight());
    assertEquals(description, chaosGame.getDescription());
  }

  /**
   * Tests the runSteps method.
   */
  @Test
  void testRunSteps() {
    chaosGame.runSteps(10);
    // We cannot predict exact outcomes without a fixed seed or checking the random behavior, but we can ensure the canvas was used
    assertTrue(Arrays.stream(chaosGame.getCanvas().getCanvasArray()).flatMapToInt(Arrays::stream).sum() > 0);
  }

  /**
   * Tests a ChaosGame without any transforms.
   */
  @Test
  void testNoTransforms() {
    ChaosGame gameWithNoTransforms = new ChaosGame(new ChaosGameDescription(List.of(), minCoords, maxCoords), 100, 100);
    gameWithNoTransforms.runSteps(10);
    // Ensuring no changes were made to the canvas because there are no transforms
    assertEquals(0, Arrays.stream(gameWithNoTransforms.getCanvas().getCanvasArray()).flatMapToInt(Arrays::stream).sum());
  }
}
