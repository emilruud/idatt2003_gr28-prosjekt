package edu.ntnu.idatt2003.model;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the ChaosCanvas class.
 */
public class ChaosCanvasTest {
  private ChaosCanvas canvas;

  /**
   * Set up a new canvas before each test.
   */
  @Before
  public void setUp() {
    canvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
  }

  /**
   * Test the initial state of the canvas.
   */
  @Test
  public void testInitialCanvasState() {
    assertEquals(100, canvas.getWidth());
    assertEquals(100, canvas.getHeight());
    assertNotNull(canvas.getCanvasArray());
  }

  /**
   * Test that the putPixel and getPixel methods work as expected.
   */
  @Test
  public void testPutAndGetPixel() {
    Vector2D point = new Vector2D(0.5, 0.5);
    canvas.putPixel(point);
    assertEquals(1, canvas.getPixel(point));
    assertEquals(-1, canvas.getPixel(new Vector2D(-1, -1)));  // Test out of bounds
  }

  /**
   * Test that the clear method resets the canvas.
   */
  @Test
  public void testClear() {
    Vector2D point = new Vector2D(0.5, 0.5);
    canvas.putPixel(point);
    canvas.clear();
    assertEquals(0, canvas.getPixel(point));
  }

  /**
   * Test that the getWidth and getHeight methods work as expected.
   */
  @Test
  public void testTransformationAccuracy() {
    // Place a pixel at a specific point.
    Vector2D point = new Vector2D(0.5, 0.5);
    canvas.putPixel(point);

    // Check the placement of the pixel.
    // Assuming the transform maps (0.5, 0.5) to the center of the canvas.
    int expectedX = canvas.getWidth() / 2;
    int expectedY = canvas.getHeight() / 2;
    assertEquals(1, canvas.getPixel(new Vector2D(0.5, 0.5)));
    // Ensure other locations are unaffected.
    assertEquals(0, canvas.getPixel(new Vector2D(0.5, 0.49)));
    assertEquals(0, canvas.getPixel(new Vector2D(0.49, 0.5)));
  }

  /**
   * Test that the toAscii method works as expected.
   */
  @Test
  public void testToAscii() {
    Vector2D point = new Vector2D(0.5, 0.5);
    canvas.putPixel(point);
    String ascii = canvas.toAscii();
    assertTrue(ascii.contains("X"));
  }
}
