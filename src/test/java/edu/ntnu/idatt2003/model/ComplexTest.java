package edu.ntnu.idatt2003.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for the Complex class.
 */
public class ComplexTest {
  private Complex complex;

  /**
   * Sets up the test objects before each test.
   */
  @BeforeEach
  public void setUp() {
    complex = new Complex(1, 2);
  }

  @Nested
  @DisplayName("Positive tests for the Complex class.")
  class PositiveComplexTest {

    /**
     * Tests the sqrt method
     */
    @Test
    void testSqrt() {
      Complex result = complex.sqrt();
      assertEquals(1.272, result.getX0(), 0.001);
      assertEquals(0.786, result.getX1(), 0.001);
    }
  }
}
