package edu.ntnu.idatt2003.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for the AffineTransform2d class.
 */
public class AffineTransform2dTest {
  private Matrix2x2 matrix;
  private Vector2D vector;
  private Vector2D point;

  /**
   * Sets up the test objects before each test.
   */
  @BeforeEach
  public void setUp() {
    matrix = new Matrix2x2(1, 2, 3, 4);
    vector = new Vector2D(1.0, 2.0);
    point = new Vector2D(1.0, 2.0);
  }

  /**
   * Tests the transform method.
   */
  @Test
  void testTransform() {
    AffineTransform2D transform = new AffineTransform2D(matrix, point);
    Vector2D result = transform.transform(vector);

    assertEquals(6, result.getX0());
    assertEquals(13, result.getX1());
  }

  /**
   * Tests the getMatrix method.
   */
  @Test
  void testGetMatrix() {
    AffineTransform2D transform = new AffineTransform2D(matrix, point);
    assertEquals(matrix, transform.getMatrix());
  }

  /**
   * Tests the getVector method.
   */
  @Test
  void testGetVector() {
    AffineTransform2D transform = new AffineTransform2D(matrix, point);
    assertEquals(point, transform.getVector());
  }

  /**
   * Tests the toFormattedString method.
   */
  @Test
  void testToFormattedString() {
    AffineTransform2D transform = new AffineTransform2D(matrix, point);
    String result = transform.toFormattedString();
    assertEquals("1.00, 2.00, 3.00, 4.00, 1.00, 2.00", result);
  }

  /**
   * Tests the getTransformType method.
   */
  @Test
  void getTransformType() {
    AffineTransform2D transform = new AffineTransform2D(matrix, point);
    assertEquals("Affine2D", transform.getTransformType());
  }
}
