package edu.ntnu.idatt2003.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the ChaosGameDescriptionFactory class.
 */
public class ChaosGameDescriptionFactoryTest {

  /**
   * Tests the createSierpinskiTriangle method.
   */
  @Test
  public void testCreateSierpinskiTriangle() {
    ChaosGameDescription description = ChaosGameDescriptionFactory.createSierpinskiTriangle();
    assertNotNull(description);
    assertEquals(3, description.getTransforms().size());
    assertInstanceOf(AffineTransform2D.class, description.getTransforms().getFirst());
  }

  /**
   * Tests the createBarnsleyFern method.
   */
  @Test
  public void testCreateBarnsleyFern() {
    ChaosGameDescription description = ChaosGameDescriptionFactory.createBarnsleyFern();
    assertNotNull(description);
    assertEquals(4, description.getTransforms().size());
    assertInstanceOf(AffineTransform2D.class, description.getTransforms().getFirst());
  }

  /**
   * Tests the createJuliaSet method.
   */
  @Test
  public void testCreateJuliaSet() {
    Complex c = new Complex(0.285, 0.01);
    ChaosGameDescription description = ChaosGameDescriptionFactory.createJuliaSet(c, 1);
    assertNotNull(description);
    assertEquals(2, description.getTransforms().size());
    assertInstanceOf(JuliaTransform.class, description.getTransforms().getFirst());
  }
}