package edu.ntnu.idatt2003.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Test class for the Vector2D class.
 */
public class Vector2dTest {
private Vector2D vector1;
private Vector2D vector2;

/**
 * Sets up the test objects before each test.
 */
@BeforeEach
void setUp() {
  vector1 = new Vector2D(1, 2);
  vector2 = new Vector2D(3, 4);
}
@Nested
@DisplayName("Positive tests for the Vector2d class.")
  class PositiveVector2dTests {

  /**
   * Tests the add method.
   */
  @Test
  void testAdd() {
    Vector2D result = vector1.add(vector2);
    assertEquals(4, result.getX0());
    assertEquals(6, result.getX1());
  }

  /**
   * Tests the subtract method.
   */
  @Test
  void testSubtract() {
    Vector2D result = vector1.subtract(vector2);
    assertEquals(-2, result.getX0());
    assertEquals(-2, result.getX1());
  }
  @Nested
  @DisplayName("Negative tests for the Vector2d class.")
  class NegativeVector2dTests {

  }
}
}

