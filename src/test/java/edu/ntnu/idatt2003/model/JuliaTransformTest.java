package edu.ntnu.idatt2003.model;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;

/**
 * Test class for the JuliaTransform class.
 */
public class JuliaTransformTest {

  private JuliaTransform juliaTransformPos;
  private JuliaTransform juliaTransformNeg;
  private Complex testPoint;

  /**
   * Set up a new JuliaTransform before each test.
   */
  @Before
  public void setUp() {
    testPoint = new Complex(0.285, 0.01);
    juliaTransformPos = new JuliaTransform(testPoint, 1);
    juliaTransformNeg = new JuliaTransform(testPoint, -1);
  }

  @Nested
  @DisplayName("Positive tests for the JuliaTransform class.")
  class PositiveJuliaTransformTest {
    /**
     * Tests the constructor of the JuliaTransform class.
     */
    @Test
    public void testConstructor_ValidSign() {
      assertNotNull("JuliaTransform should not be null with sign 1", juliaTransformPos);
      assertNotNull("JuliaTransform should not be null with sign -1", juliaTransformNeg);
    }


    /**
     * Tests the transform method with a positive sign.
     */
    @Test
    public void testTransform_PositiveSign() {
      Vector2D z = new Vector2D(0.5, 0.5);
      Vector2D result = juliaTransformPos.transform(z);
      Complex expected = new Complex(z.getX0() - testPoint.getReal(), z.getX1() - testPoint.getImaginary()).sqrt();
      assertEquals("The transformed X coordinate does not match", expected.getReal(), result.getX0(), 0.001);
      assertEquals("The transformed Y coordinate does not match", expected.getImaginary(), result.getX1(), 0.001);
    }

    /**
     * Tests the transform method with a negative sign.
     */
    @Test
    public void testTransform_NegativeSign() {
      Vector2D z = new Vector2D(0.5, 0.5);
      Vector2D result = juliaTransformNeg.transform(z);
      Complex expected = new Complex(z.getX0() - testPoint.getReal(), z.getX1() - testPoint.getImaginary()).sqrt();
      assertEquals("The transformed X coordinate does not match for negative sign", -expected.getReal(), result.getX0(), 0.001);
      assertEquals("The transformed Y coordinate does not match for negative sign", -expected.getImaginary(), result.getX1(), 0.001);
    }

    /**
     * Tests the getTransformType method.
     */
    @Test
    public void testGetTransformType() {
      String result = juliaTransformNeg.getTransformType();
      assertEquals("Julia", result);
    }

    /**
     * Tests the toFormattedString method.
     */
    @Test
    public void testToFormattedString() {
      String result = juliaTransformNeg.toFormattedString();
      assertEquals("0.28500, 0.01000", result);
    }

    /**
     * Tests the getPoint method.
     */
    @Test
    public void testGetPoint() {
      Complex result = juliaTransformNeg.getPoint();
      assertEquals(testPoint, result);
    }
  }

  @Nested
  @DisplayName("Negative tests for the JuliaTransform class.")
  class NegativeJuliaTransformTest {

    /**
     * Tests the constructor of the JuliaTransform class with an invalid sign.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_InvalidSign() {
      new JuliaTransform(testPoint, 0);
    }
  }
}
