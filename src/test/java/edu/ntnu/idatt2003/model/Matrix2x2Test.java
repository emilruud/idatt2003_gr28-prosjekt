package edu.ntnu.idatt2003.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Test class for the Matrix2x2 class.
 */
public class Matrix2x2Test {
  private Matrix2x2 matrix;
  private Vector2D vector;

  @BeforeEach
  void setUp() {
    matrix = new Matrix2x2(1, 2, 3, 4);
    vector = new Vector2D(1, 2);
  }

  @Nested
  @DisplayName("Positive tests for the Matrix2x2 class.")
  class PositiveMatrix2x2Test {

    /**
     * Tests the multiply method.
     */
    @Test
    void testMultiply() {
      Vector2D result = matrix.multiply(vector);
      assertEquals(5, result.getX0());
      assertEquals(11, result.getX1());
    }

    /**
     * Tests the getA00 method.
     */
    @Test
    void testGetA00() {
      assertEquals(1, matrix.getA00());
    }

    /**
     * Tests the getA01 method.
     */
    @Test
    void testGetA01() {
      assertEquals(2, matrix.getA01());
    }

    /**
     * Tests the getA10 method.
     */
    @Test
    void testGetA10() {
      assertEquals(3, matrix.getA10());
    }

    /**
     * Tests the getA11 method.
     */
    @Test
    void testGetA11() {
      assertEquals(4, matrix.getA11());
    }
  }
  @Nested
  @DisplayName("Negative tests for the Matrix2x2 class.")
  class NegativeMatrix2x2Test {

  }
}
